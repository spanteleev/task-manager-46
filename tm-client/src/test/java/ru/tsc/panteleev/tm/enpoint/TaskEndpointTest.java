package ru.tsc.panteleev.tm.enpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.panteleev.tm.api.endpoint.*;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.dto.request.project.ProjectCreateRequest;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.request.user.UserLoginRequest;
import ru.tsc.panteleev.tm.dto.request.user.UserRegistryRequest;
import ru.tsc.panteleev.tm.dto.response.project.ProjectCreateResponse;
import ru.tsc.panteleev.tm.dto.response.task.TaskCreateResponse;
import ru.tsc.panteleev.tm.dto.response.task.TaskListResponse;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.marker.SoapCategory;
import ru.tsc.panteleev.tm.dto.model.ProjectDTO;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.service.PropertyService;

import java.util.UUID;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private String userToken;

    @Before
    public void getToken() {
        userToken = authEndpoint.login(new UserLoginRequest("test", "test")).getToken();
    }

    @After
    public void clear() {
        userToken = null;
    }

    @Test
    public void createTask() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(new TaskCreateRequest()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest("", "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(UUID.randomUUID().toString(), "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(userToken, "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(userToken, null, "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(userToken, "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.createTask(
                        new TaskCreateRequest(userToken, "", null, null, null)));
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, name, description, null, null));
        @Nullable final TaskDTO task = response.getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void listTask() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.listTask(new TaskListRequest("", null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.listTask(new TaskListRequest(UUID.randomUUID().toString(), null)));
        @Nullable final TaskListResponse response =
                taskEndpoint.listTask(new TaskListRequest(userToken, null));
        Assert.assertNotNull(response.getTasks());
    }

    @Test
    public void clearTask() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        userEndpoint.registrationUser(new UserRegistryRequest(login, password, email));
        @Nullable final String token = authEndpoint.login(new UserLoginRequest(login, password)).getToken();
        taskEndpoint.createTask(
                new TaskCreateRequest(token, UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null));
        taskEndpoint.createTask(
                new TaskCreateRequest(token, UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null));
        Assert.assertEquals(2,
                taskEndpoint.listTask(new TaskListRequest(token, null)).getTasks().size());
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.clearTask(new TaskClearRequest()));
        taskEndpoint.clearTask(new TaskClearRequest(token));
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(token, null)).getTasks());
    }

    @Test
    public void showTask() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse response =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, name, description, null, null));
        @Nullable TaskDTO task = response.getTask();
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.showTaskById(new TaskGetByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskById(new TaskGetByIdRequest("", "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showTaskById(new TaskGetByIdRequest(userToken, "")));
        task = taskEndpoint.showTaskById(new TaskGetByIdRequest(userToken, taskId)).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

    @Test
    public void updateTask() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse response =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, name, description, null, null));
        @Nullable TaskDTO task = response.getTask();
        @NotNull final String taskId = task.getId();
        @NotNull final String newName = UUID.randomUUID().toString();
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest("", "", "", "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(userToken, "", "", "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(userToken, null, "", "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(userToken, taskId, null, "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(userToken, null, newName, "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(userToken, taskId, newName, null)));
        task = taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(userToken, taskId, newName, newDescription)).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test
    public void removeTask() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse response =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, name, description, null, null));
        @Nullable TaskDTO task = response.getTask();
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest("", "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(userToken, "")));
        @NotNull final String removeTaskId =
                taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(userToken, taskId)).getTask().getId();
        Assert.assertEquals(taskId, removeTaskId);
    }

    @Test
    public void changeTaskStatus() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse response =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, name, description, null, null));
        @Nullable TaskDTO task = response.getTask();
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest("", "", null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(userToken, "", null)));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(userToken, taskId, null)));
        task = taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(userToken, taskId, Status.IN_PROGRESS)).getTask();
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
        task = taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(userToken, taskId, Status.COMPLETED)).getTask();
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void startTask() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse response =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, name, description, null, null));
        @Nullable TaskDTO task = response.getTask();
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.startTaskById(new TaskStartByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.startTaskById(new TaskStartByIdRequest("", "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.startTaskById(new TaskStartByIdRequest(userToken, "")));
        task = taskEndpoint.startTaskById(new TaskStartByIdRequest(userToken, taskId)).getTask();
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void completeTask() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse response =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, name, description, null, null));
        @Nullable TaskDTO task = response.getTask();
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest("", "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(userToken, "")));
        task = taskEndpoint.completeTaskStatusById(new TaskCompleteByIdRequest(userToken, taskId)).getTask();
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable final ProjectDTO project = response.getProject();
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse taskResponse =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, taskName, taskDescription, null, null));
        @Nullable final TaskDTO task = taskResponse.getTask();
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindToProjectTaskById(new TaskBindToProjectRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindToProjectTaskById(new TaskBindToProjectRequest("", "", "")));
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindToProjectTaskById(new TaskBindToProjectRequest(userToken, "", "")));
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.bindToProjectTaskById(new TaskBindToProjectRequest(userToken, project.getId(), "")));
        projectTaskEndpoint.bindToProjectTaskById(new TaskBindToProjectRequest(userToken, project.getId(), task.getId()));
        @Nullable TaskDTO task1 = taskEndpoint.showTaskById(new TaskGetByIdRequest(userToken, task.getId())).getTask();
        Assert.assertEquals(project.getId(), task1.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable final ProjectDTO project = response.getProject();
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse taskResponse =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, taskName, taskDescription, null, null));
        @Nullable final TaskDTO task = taskResponse.getTask();
        projectTaskEndpoint.bindToProjectTaskById(new TaskBindToProjectRequest(userToken, project.getId(), task.getId()));
        @Nullable TaskDTO task1 = taskEndpoint.showTaskById(new TaskGetByIdRequest(userToken, task.getId())).getTask();
        Assert.assertEquals(project.getId(), task1.getProjectId());
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindFromProjectTaskById(new TaskUnbindFromProjectRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindFromProjectTaskById(new TaskUnbindFromProjectRequest("", "", "")));
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindFromProjectTaskById(new TaskUnbindFromProjectRequest(userToken, "", "")));
        Assert.assertThrows(Exception.class,
                () -> projectTaskEndpoint.unbindFromProjectTaskById(new TaskUnbindFromProjectRequest(userToken, project.getId(), "")));
        projectTaskEndpoint.unbindFromProjectTaskById(new TaskUnbindFromProjectRequest(userToken, project.getId(), task.getId()));
        task1 = taskEndpoint.showTaskById(new TaskGetByIdRequest(userToken, task.getId())).getTask();
        Assert.assertNull(task1.getProjectId());
    }

    @Test
    public void listTaskByProjectId() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable final ProjectDTO project = response.getProject();
        @NotNull final String taskName1 = UUID.randomUUID().toString();
        @NotNull final String taskDescription1 = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse taskResponse1 =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, taskName1, taskDescription1, null, null));
        @Nullable final TaskDTO task1 = taskResponse1.getTask();
        @NotNull final String taskName2 = UUID.randomUUID().toString();
        @NotNull final String taskDescription2 = UUID.randomUUID().toString();
        @Nullable final TaskCreateResponse taskResponse2 =
                taskEndpoint.createTask(new TaskCreateRequest(userToken, taskName2, taskDescription2, null, null));
        @Nullable final TaskDTO task2 = taskResponse2.getTask();
        projectTaskEndpoint.bindToProjectTaskById(new TaskBindToProjectRequest(userToken, project.getId(), task1.getId()));
        projectTaskEndpoint.bindToProjectTaskById(new TaskBindToProjectRequest(userToken, project.getId(), task2.getId()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showListByProjectIdTask(new TaskGetListByProjectIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showListByProjectIdTask(new TaskGetListByProjectIdRequest("", "")));
        Assert.assertThrows(Exception.class,
                () -> taskEndpoint.showListByProjectIdTask(new TaskGetListByProjectIdRequest(userToken, "")));
        Assert.assertEquals(2,
                taskEndpoint.showListByProjectIdTask(new TaskGetListByProjectIdRequest(userToken, project.getId())).getTasks().size());
    }

}
