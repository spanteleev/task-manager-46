package ru.tsc.panteleev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.panteleev.tm.dto.model.ProjectDTO;
import ru.tsc.panteleev.tm.enumerated.Sort;
import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class ProjectRepositoryDTO extends AbstractUserOwnedRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void set(@NotNull Collection<ProjectDTO> projects) {
        clear();
        for (ProjectDTO project : projects)
            add(project);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(@NotNull String userId) {
        return entityManager.createQuery("FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort) {
        if (sort == null) return findAllByUserId(userId);
        @NotNull final String query =
                String.format("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.%s", getSortColumn(sort));
        return entityManager.createQuery(query, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @NotNull List<ProjectDTO> findAll() {
        return entityManager.createQuery("FROM ProjectDTO", ProjectDTO.class).getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        remove(findById(userId, id));
    }

    @Override
    public void clearByUserId(@NotNull String userId) {
        @NotNull final List<ProjectDTO> projects = findAllByUserId(userId);
        for (ProjectDTO project : projects)
            remove(project);
    }

    @Override
    public void clear() {
        @NotNull final List<ProjectDTO> projects = findAll();
        for (ProjectDTO project : projects)
            remove(project);
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(p) FROM ProjectDTO p", Long.class).getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return findById(userId, id) != null;
    }

}
