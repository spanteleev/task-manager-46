package ru.tsc.panteleev.tm.exception.user;

import ru.tsc.panteleev.tm.exception.AbstractException;

public final class LoginOrPasswordIncorrectException extends AbstractException {

    public LoginOrPasswordIncorrectException() {
        super("Error! Login or password is incorrect. Please try again...");
    }

}
